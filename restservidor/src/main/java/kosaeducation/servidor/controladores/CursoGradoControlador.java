package kosaeducation.servidor.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kosaeducation.servidor.modelos.cursogrado.CursoGradoDto;
import kosaeducation.servidor.servicios.cursogrado.ServicioCursogrado;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/cursosgrados")
public class CursoGradoControlador {

	@Autowired
	private ServicioCursogrado servicio;

	@RequestMapping(method = RequestMethod.GET)
	public List<CursoGradoDto> listarTodos() {
		return this.getServicio().listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public CursoGradoDto buscarPorId(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorId(id);
	}

	@RequestMapping(path = "/curso/{idCurso}", method = RequestMethod.GET)
	public List<CursoGradoDto> buscarPorCurso(@PathVariable("idCurso") String idCurso) {
		return this.getServicio().buscarPorCurso(idCurso);
	}
	
	@RequestMapping(path = "/grado/{idGrado}", method = RequestMethod.GET)
	public List<CursoGradoDto> buscarPorGrado(@PathVariable("idGrado") String idGrado) {
		return this.getServicio().buscarPorGrado(idGrado);
	}

	@RequestMapping(path = "/nuevo", method = RequestMethod.POST)
	public CursoGradoDto crear(@RequestBody CursoGradoDto dto) {
		return this.getServicio().guardarNuevo(dto);
	}
	
	@RequestMapping(path = "/editar", method = RequestMethod.PUT)
	public CursoGradoDto editar(@RequestBody CursoGradoDto dto) {
		return this.getServicio().editar(dto);
	}
	
	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable("id") Integer id) {
		this.getServicio().eliminiar(this.getServicio().buscarPorId(id));
	}

}
