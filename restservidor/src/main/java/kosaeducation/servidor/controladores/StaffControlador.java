package kosaeducation.servidor.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kosaeducation.servidor.modelos.staff.StaffDto;
import kosaeducation.servidor.servicios.staff.ServicioStaff;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/staffs")
public class StaffControlador {

	@Autowired
	private ServicioStaff servicio;

	@RequestMapping(method = RequestMethod.GET)
	public List<StaffDto> listarTodos() {
		return this.getServicio().listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public StaffDto buscarPorId(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorId(id);
	}

	@RequestMapping(path = "/nombre/{nombre}", method = RequestMethod.GET)
	public List<StaffDto> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return this.getServicio().buscarPorNombre(nombre);
	}

	@RequestMapping(path = "/nuevo/", method = RequestMethod.POST)
	public StaffDto crear(@RequestBody StaffDto dto) {
		return this.getServicio().guardarNuevo(dto);
	}
	
	@RequestMapping(path = "/editar/", method = RequestMethod.PUT)
	public StaffDto editar(@RequestBody StaffDto dto) {
		return this.getServicio().editar(dto);
	}
	
	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable("id") Integer id) {
		this.getServicio().eliminiar(this.getServicio().buscarPorId(id));
	}

}
