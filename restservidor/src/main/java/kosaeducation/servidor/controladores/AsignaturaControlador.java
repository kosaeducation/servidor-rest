package kosaeducation.servidor.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kosaeducation.servidor.modelos.asignatura.AsignaturaDto;
import kosaeducation.servidor.servicios.asignatura.ServicioAsignatura;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/asignaturas")
public class AsignaturaControlador {

	@Autowired
	private ServicioAsignatura servicio;

	@RequestMapping(method = RequestMethod.GET)
	public List<AsignaturaDto> listarTodos() {
		return this.getServicio().listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public AsignaturaDto buscarPorId(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorId(id);
	}

	@RequestMapping(path = "/cursogrado/{id}", method = RequestMethod.GET)
	public List<AsignaturaDto> buscarPorCursoGrado(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorCursoGrado(id);
	}

	@RequestMapping(path = "/grado/{id}", method = RequestMethod.GET)
	public List<AsignaturaDto> buscarPorGrado(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorGrado(id);
	}

	@RequestMapping(path = "/profesor/{id}", method = RequestMethod.GET)
	public List<AsignaturaDto> buscarPorProfesor(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorProfesor(id);
	}

	@RequestMapping(path = "/nombre/{nombre}", method = RequestMethod.GET)
	public AsignaturaDto buscarPorProfesor(@PathVariable("nombre") String nombre) {
		return this.getServicio().buscarPorNombre(nombre);
	}

	@RequestMapping(path = "/nuevo", method = RequestMethod.POST)
	public AsignaturaDto crear(@RequestBody AsignaturaDto dto) {
		return this.getServicio().guardarNuevo(dto);
	}

	@RequestMapping(path = "/editar", method = RequestMethod.PUT)
	public AsignaturaDto editar(@RequestBody AsignaturaDto dto) {
		return this.getServicio().editar(dto);
	}

	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable("id") Integer id) {
		this.getServicio().eliminiar(this.getServicio().buscarPorId(id));
	}

}
