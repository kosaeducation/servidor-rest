package kosaeducation.servidor.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kosaeducation.servidor.modelos.curso.CursoDto;
import kosaeducation.servidor.servicios.curso.ServicioCurso;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/cursos")
public class CursoControlador {

	@Autowired
	private ServicioCurso servicio;

	@RequestMapping(method = RequestMethod.GET)
	public List<CursoDto> listarTodos() {
		return this.getServicio().listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public CursoDto buscarPorId(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorId(id);
	}

	@RequestMapping(path = "/numero/{numero}", method = RequestMethod.GET)
	public List<CursoDto> buscarPorNumero(@PathVariable("numero") String numero) {
		return this.getServicio().buscarPorNumeroCurso(numero);
	}

	@RequestMapping(path = "/nuevo", method = RequestMethod.POST)
	public CursoDto crear(@RequestBody CursoDto dto) {
		return this.getServicio().guardarNuevo(dto);
	}
	
	@RequestMapping(path = "/editar", method = RequestMethod.PUT)
	public CursoDto editar(@RequestBody CursoDto dto) {
		return this.getServicio().editar(dto);
	}
	
	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable("id") Integer id) {
		this.getServicio().eliminiar(this.getServicio().buscarPorId(id));
	}

}
