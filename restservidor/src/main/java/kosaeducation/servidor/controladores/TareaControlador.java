package kosaeducation.servidor.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kosaeducation.servidor.modelos.asignatura.AsignaturaDto;
import kosaeducation.servidor.modelos.tarea.TareaDto;
import kosaeducation.servidor.servicios.tarea.ServicioTarea;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/tareas")
public class TareaControlador {

	@Autowired
	private ServicioTarea servicio;

	@RequestMapping(method = RequestMethod.GET)
	public List<TareaDto> listarTodos() {
		return this.getServicio().listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public TareaDto buscarPorId(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorId(id);
	}

	@RequestMapping(path = "/asignatura/{idAsignatura}", method = RequestMethod.GET)
	public List<TareaDto> buscarPorNumero(@PathVariable("idAsignatura") Integer idAsignatura) {
		return this.getServicio().buscarPorAsignatura(idAsignatura);
	}

	@RequestMapping(path = "/nuevo", method = RequestMethod.POST)
	public TareaDto crear(@RequestBody TareaDto dto) {
		return this.getServicio().guardarNuevo(dto);
	}
	
	@RequestMapping(path = "/editar", method = RequestMethod.PUT)
	public TareaDto editar(@RequestBody TareaDto dto) {
		return this.getServicio().editar(dto);
	}
	
	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable("id") Integer id) {
		this.getServicio().eliminiar(this.getServicio().buscarPorId(id));
	}

}
