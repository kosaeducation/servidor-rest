package kosaeducation.servidor.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kosaeducation.servidor.modelos.grado.GradoDto;
import kosaeducation.servidor.servicios.grado.ServicioGrado;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/grados")
public class GradoControlador {

	@Autowired
	private ServicioGrado servicio;

	@RequestMapping(method = RequestMethod.GET)
	public List<GradoDto> listarTodos() {
		return this.getServicio().listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public GradoDto buscarPorId(@PathVariable("id") Integer id) {
		return this.getServicio().buscarPorId(id);
	}

	@RequestMapping(path = "/nombre/{nombre}", method = RequestMethod.GET)
	public List<GradoDto> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return this.getServicio().buscarPorNombre(nombre);
	}

	@RequestMapping(path = "/nuevo", method = RequestMethod.POST)
	public GradoDto crear(@RequestBody GradoDto dto) {
		return this.getServicio().guardarNuevo(dto);
	}

	@RequestMapping(path = "/editar", method = RequestMethod.PUT)
	public GradoDto editar(@RequestBody GradoDto dto) {
		return this.getServicio().editar(dto);
	}

	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable("id") Integer id) {
		this.getServicio().eliminiar(this.getServicio().buscarPorId(id));
	}

}
