package kosaeducation.servidor.utilidades;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EntidadDto<ENTIDAD, DTO> {

	private ENTIDAD entidad;
	private DTO dto;
	
	ModelMapper mapeador = new ModelMapper();
	List<DTO> listaDto;
	
	public EntidadDto(ENTIDAD e, DTO d) {
		this.entidad = e;
		this.dto = d;
	}
	
	@SuppressWarnings("unchecked")
	public DTO aDto(ENTIDAD aconvertir) {
		System.out.println("==== CONVIRTIENDO A DTO... ====");
		return (DTO) mapeador.map(aconvertir, this.getDto().getClass());
	}


	@SuppressWarnings("unchecked")
	public ENTIDAD aEntidad(DTO aconvertir) {
		return (ENTIDAD) mapeador.map(aconvertir, this.getEntidad().getClass());
	}

	
	@SuppressWarnings("unchecked")
	public List<DTO> aDtoLista(List<ENTIDAD> lista) {

		listaDto = new ArrayList<>();
		
		for (ENTIDAD entidad : lista) {
			System.out.println(entidad);
			this.getListaDto().add((DTO) mapeador.map(entidad, this.getDto().getClass()));
		}

		return listaDto;
	}
}