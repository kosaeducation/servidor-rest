package kosaeducation.servidor.upload;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import kosaeducation.servidor.modelos.tarea.TareaDto;

@RestController
public class FileController {
	@Autowired
	private FileService fileservice;

	List<String> files = new ArrayList<String>();

	@PostMapping("/subidas")
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile[] file,
			@RequestPart("tarea") TareaDto tarea) {
		String message = "";
		try {
			fileservice.store(file, tarea);
			// message = "You successfully uploaded " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			// message = "Fail to upload Profile Picture" + file.getOriginalFilename() +
			// "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}
	}

	@GetMapping("/bajadas")
	public ResponseEntity<List<String>> getListFiles(Model model) {
		System.out.println("#########################################################");
		System.out.println("#########################################################");
		System.out.println("#########################################################");
		List<String> fileNames = files
				.stream().map(fileName -> MvcUriComponentsBuilder
						.fromMethodName(FileController.class, "getFile", fileName).build().toString())
				.collect(Collectors.toList());

		return ResponseEntity.ok().body(fileNames);
	}

	@GetMapping("/bajadas/1")
	@ResponseBody
	public ResponseEntity<Resource> getFile() {

		System.out.println("==============================================");
		System.out.println("==============================================");
		System.out.println("==============================================");
		String filename = "wallpaper2.png";
		Resource file = fileservice.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

}