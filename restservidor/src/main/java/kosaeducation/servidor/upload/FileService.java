package kosaeducation.servidor.upload;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import kosaeducation.servidor.modelos.recursotarea.RecursoTareaDto;
import kosaeducation.servidor.modelos.tarea.TareaDto;
import kosaeducation.servidor.servicios.recursotarea.ServicioRecursotarea;

@Service
public class FileService {
	private final Path rootLocation = Paths.get("C:\\imgs");

	@Autowired
	private ServicioRecursotarea servicio;

	public void store(MultipartFile[] file, TareaDto tarea) {
		try {
			for (MultipartFile multipartFile : file) {
				System.out.println(multipartFile.getOriginalFilename());
				System.out.println(rootLocation.toUri());
				this.servicio.guardarNuevo(new RecursoTareaDto(0, rootLocation.toString(),
						multipartFile.getOriginalFilename(), tarea));
				Files.copy(multipartFile.getInputStream(),
						this.rootLocation.resolve(multipartFile.getOriginalFilename()));
			}

		} catch (Exception e) {
			throw new RuntimeException("FAIL!");
		}
	}

	public Resource loadFile(String filename) {
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");
		try {
			Path file = rootLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("FAIL!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("FAIL!");
		}
	}
}