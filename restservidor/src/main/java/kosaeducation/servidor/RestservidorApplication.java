package kosaeducation.servidor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestservidorApplication {


	public static void main(String[] args) {
		SpringApplication.run(RestservidorApplication.class, args);
	}

}
