package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.curso.Curso;
import kosaeducation.servidor.modelos.cursogrado.CursoGrado;
import kosaeducation.servidor.modelos.grado.Grado;

@Repository
public interface ICursoGradoDao extends IGenericoDao<CursoGrado> {

	List<CursoGrado> findByCurso(Curso curso);
	
	List<CursoGrado> findByGrado(Grado grado);
	
}
