package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.curso.Curso;

@Repository
public interface ICursoDao extends IGenericoDao<Curso> {

	List<Curso> findByNumCurso(String numCurso);
	
}
