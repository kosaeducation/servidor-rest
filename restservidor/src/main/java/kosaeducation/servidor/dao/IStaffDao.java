package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.staff.Staff;

@Repository
public interface IStaffDao extends IGenericoDao<Staff>{

	List<Staff> findByNombre(String nombre);
	
}
