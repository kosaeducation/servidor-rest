package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.recursotarea.RecursoTarea;
import kosaeducation.servidor.modelos.tarea.Tarea;

@Repository
public interface IRecursoTareaDao extends IGenericoDao<RecursoTarea> {

	List<RecursoTarea> findByTarea(Tarea tarea);

}
