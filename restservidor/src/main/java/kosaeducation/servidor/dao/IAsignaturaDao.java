package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.asignatura.Asignatura;

@Repository
public interface IAsignaturaDao extends IGenericoDao<Asignatura> {

	@Query("SELECT asignatura FROM Asignatura asignatura WHERE asignatura.cursogrado.idCursoGrado = :idCursoGrado")
	List<Asignatura> findByCursogrado(Integer idCursoGrado);
	
	@Query("SELECT asignatura FROM Asignatura asignatura WHERE asignatura.cursogrado.grado.idGrado = :idGrado")
	List<Asignatura> findByGrado(Integer idGrado);
	
	Asignatura findByNombre(String nombre);
	
	@Query("SELECT asignatura FROM Asignatura asignatura WHERE asignatura.profesor.idStaff = :idProfesor")
	List<Asignatura> findByProfesor(Integer idProfesor);
	
}
