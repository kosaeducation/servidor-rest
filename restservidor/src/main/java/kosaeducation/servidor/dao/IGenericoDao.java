package kosaeducation.servidor.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IGenericoDao<T> extends JpaRepository<T, Integer>{

}
