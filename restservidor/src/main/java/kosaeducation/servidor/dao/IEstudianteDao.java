package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.estudiante.Estudiante;

@Repository
public interface IEstudianteDao extends IGenericoDao<Estudiante>{

	List<Estudiante> findByNombre(String nombre);
	
}
