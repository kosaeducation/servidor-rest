package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.grado.Grado;

@Repository
public interface IGradoDao extends IGenericoDao<Grado> {

	List<Grado> findByNombre(String nombre);

}
