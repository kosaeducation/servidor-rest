package kosaeducation.servidor.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kosaeducation.servidor.modelos.asignatura.Asignatura;
import kosaeducation.servidor.modelos.tarea.Tarea;

@Repository
public interface ITareaDao extends IGenericoDao<Tarea> {

	List<Tarea> findByAsignatura(Asignatura asignatura);
}
