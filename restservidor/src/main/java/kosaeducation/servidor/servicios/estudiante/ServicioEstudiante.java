package kosaeducation.servidor.servicios.estudiante;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.IEstudianteDao;
import kosaeducation.servidor.modelos.estudiante.Estudiante;
import kosaeducation.servidor.modelos.estudiante.EstudianteDto;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioEstudiante implements IServicioEstudiante {

	@Autowired
	private IEstudianteDao dao;

	List<EstudianteDto> listadto;

	EntidadDto<Estudiante, EstudianteDto> conversor = new EntidadDto<>(new Estudiante(), new EstudianteDto());

	public List<EstudianteDto> listarTodos() {

		return conversor.aDtoLista(this.getDao().findAll());
	}

	@Override
	public EstudianteDto buscarPorId(Integer id) {

		return conversor.aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<EstudianteDto> buscarPorNombre(String nombre) {
		return conversor.aDtoLista(this.getDao().findByNombre(nombre));
	}

	@Override
	public EstudianteDto guardarNuevo(EstudianteDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.conversor.aEntidad(dto)));
	}

	@Override
	public EstudianteDto editar(EstudianteDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.conversor.aEntidad(dto)));
	}

	@Override
	public void eliminiar(EstudianteDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}

}
