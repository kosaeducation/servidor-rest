package kosaeducation.servidor.servicios.estudiante;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.estudiante.EstudianteDto;

@Service
public interface IServicioEstudiante {

	List<EstudianteDto> listarTodos();
	
	EstudianteDto buscarPorId(Integer id);
	
	List<EstudianteDto> buscarPorNombre(String nombre);
	
	EstudianteDto guardarNuevo(EstudianteDto dto);
	
	EstudianteDto editar(EstudianteDto dto);

	void eliminiar(EstudianteDto dto);
}
