package kosaeducation.servidor.servicios.curso;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.curso.CursoDto;

@Service
public interface IServicioCurso {

	List<CursoDto> listarTodos();
	
	CursoDto buscarPorId(Integer id);
	
	List<CursoDto> buscarPorNumeroCurso(String numero);
	
	CursoDto guardarNuevo(CursoDto dto);
	
	CursoDto editar(CursoDto dto);

	void eliminiar(CursoDto dto);
}
