package kosaeducation.servidor.servicios.curso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.ICursoDao;
import kosaeducation.servidor.modelos.curso.Curso;
import kosaeducation.servidor.modelos.curso.CursoDto;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioCurso implements IServicioCurso {

	@Autowired
	private ICursoDao dao;

	List<CursoDto> listadto;

	EntidadDto<Curso, CursoDto> conversor = new EntidadDto<>(new Curso(), new CursoDto());

	@Override
	public List<CursoDto> listarTodos() {
		return this.getConversor().aDtoLista(this.getDao().findAll());
	}

	@Override
	public CursoDto buscarPorId(Integer id) {
		return this.getConversor().aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<CursoDto> buscarPorNumeroCurso(String numero) {
		return this.getConversor().aDtoLista(this.getDao().findByNumCurso(numero));
	}

	@Override
	public CursoDto guardarNuevo(CursoDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public CursoDto editar(CursoDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public void eliminiar(CursoDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}


}
