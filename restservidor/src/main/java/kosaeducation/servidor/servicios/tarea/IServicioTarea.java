package kosaeducation.servidor.servicios.tarea;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.tarea.TareaDto;

@Service
public interface IServicioTarea {

	List<TareaDto> listarTodos();
	
	TareaDto buscarPorId(Integer id);
	
	List<TareaDto> buscarPorAsignatura(Integer asignatura);
	
	TareaDto guardarNuevo(TareaDto dto);
	
	TareaDto editar(TareaDto dto);

	void eliminiar(TareaDto dto);
}
