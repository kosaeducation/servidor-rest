package kosaeducation.servidor.servicios.tarea;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.ITareaDao;
import kosaeducation.servidor.modelos.asignatura.Asignatura;
import kosaeducation.servidor.modelos.asignatura.AsignaturaDto;
import kosaeducation.servidor.modelos.tarea.Tarea;
import kosaeducation.servidor.modelos.tarea.TareaDto;
import kosaeducation.servidor.servicios.asignatura.IServicioAsignatura;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioTarea implements IServicioTarea {

	@Autowired
	private ITareaDao dao;

	@Autowired
	private IServicioAsignatura servicioAsignatura;

	List<TareaDto> listadto;

	EntidadDto<Tarea, TareaDto> conversor = new EntidadDto<>(new Tarea(), new TareaDto());
	EntidadDto<Asignatura, AsignaturaDto> conversor2 = new EntidadDto<>(new Asignatura(), new AsignaturaDto());

	@Override
	public List<TareaDto> listarTodos() {
		return this.getConversor().aDtoLista(this.getDao().findAll());
	}

	@Override
	public TareaDto buscarPorId(Integer id) {
		return this.getConversor().aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<TareaDto> buscarPorAsignatura(Integer idAsignatura) {
		return this.getConversor().aDtoLista(this.getDao().findByAsignatura(
				this.getConversor2().aEntidad(this.getServicioAsignatura().buscarPorId(idAsignatura))));
	}

	@Override
	public TareaDto guardarNuevo(TareaDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public TareaDto editar(TareaDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public void eliminiar(TareaDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}

}
