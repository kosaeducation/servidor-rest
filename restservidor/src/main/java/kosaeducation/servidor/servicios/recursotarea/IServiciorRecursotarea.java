package kosaeducation.servidor.servicios.recursotarea;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.recursotarea.RecursoTareaDto;

@Service
public interface IServiciorRecursotarea {

	List<RecursoTareaDto> listarTodos();
	
	RecursoTareaDto buscarPorId(Integer id);
	
	List<RecursoTareaDto> buscarPorTarea(Integer idTarea);
	
	RecursoTareaDto guardarNuevo(RecursoTareaDto dto);
	
	RecursoTareaDto RecursoTareaDto(RecursoTareaDto dto);

	void eliminiar(RecursoTareaDto dto);
}
