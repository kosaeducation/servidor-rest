package kosaeducation.servidor.servicios.recursotarea;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.IRecursoTareaDao;
import kosaeducation.servidor.modelos.recursotarea.RecursoTarea;
import kosaeducation.servidor.modelos.recursotarea.RecursoTareaDto;
import kosaeducation.servidor.modelos.tarea.Tarea;
import kosaeducation.servidor.modelos.tarea.TareaDto;
import kosaeducation.servidor.servicios.tarea.IServicioTarea;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioRecursotarea implements IServiciorRecursotarea {

	@Autowired
	private IRecursoTareaDao dao;

	@Autowired
	private IServicioTarea servicioTarea;

	List<RecursoTareaDto> listadto;

	EntidadDto<RecursoTarea, RecursoTareaDto> conversor = new EntidadDto<>(new RecursoTarea(), new RecursoTareaDto());
	EntidadDto<Tarea, TareaDto> conversor2 = new EntidadDto<>(new Tarea(), new TareaDto());

	@Override
	public List<RecursoTareaDto> listarTodos() {
		return this.getConversor().aDtoLista(this.getDao().findAll());
	}

	@Override
	public RecursoTareaDto buscarPorId(Integer id) {
		return this.getConversor().aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<RecursoTareaDto> buscarPorTarea(Integer idTarea) {
		return this.getConversor().aDtoLista(
				this.getDao().findByTarea(this.getConversor2().aEntidad(this.getServicioTarea().buscarPorId(idTarea))));
	}

	@Override
	public RecursoTareaDto guardarNuevo(RecursoTareaDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public RecursoTareaDto RecursoTareaDto(RecursoTareaDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public void eliminiar(RecursoTareaDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}

}
