package kosaeducation.servidor.servicios.cursogrado;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.ICursoGradoDao;
import kosaeducation.servidor.modelos.cursogrado.CursoGrado;
import kosaeducation.servidor.modelos.cursogrado.CursoGradoDto;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioCursogrado implements IServicioCursogrado {

	@Autowired
	private ICursoGradoDao dao;

	List<CursoGradoDto> listadto;

	EntidadDto<CursoGrado, CursoGradoDto> conversor = new EntidadDto<>(new CursoGrado(), new CursoGradoDto());

	@Override
	public List<CursoGradoDto> listarTodos() {
		return this.getConversor().aDtoLista(this.getDao().findAll());
	}

	@Override
	public CursoGradoDto buscarPorId(Integer id) {
		return this.getConversor().aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<CursoGradoDto> buscarPorGrado(String grado) {
		return null;
	}

	@Override
	public List<CursoGradoDto> buscarPorCurso(String curso) {
		return null;
	}

	@Override
	public CursoGradoDto guardarNuevo(CursoGradoDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public CursoGradoDto editar(CursoGradoDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public void eliminiar(CursoGradoDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}



}
