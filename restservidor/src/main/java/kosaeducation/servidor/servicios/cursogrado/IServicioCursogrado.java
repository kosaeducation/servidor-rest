package kosaeducation.servidor.servicios.cursogrado;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.cursogrado.CursoGradoDto;

@Service
public interface IServicioCursogrado {

	List<CursoGradoDto> listarTodos();
	
	CursoGradoDto buscarPorId(Integer id);
	
	List<CursoGradoDto> buscarPorGrado(String grado);
	
	List<CursoGradoDto> buscarPorCurso(String curso);
	
	CursoGradoDto guardarNuevo(CursoGradoDto dto);
	
	CursoGradoDto editar(CursoGradoDto dto);

	void eliminiar(CursoGradoDto dto);
}
