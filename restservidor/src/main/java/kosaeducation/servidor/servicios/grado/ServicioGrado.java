package kosaeducation.servidor.servicios.grado;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.IGradoDao;
import kosaeducation.servidor.modelos.grado.Grado;
import kosaeducation.servidor.modelos.grado.GradoDto;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioGrado implements IServicioGrado {

	@Autowired
	private IGradoDao dao;

	List<GradoDto> listadto;

	EntidadDto<Grado, GradoDto> conversor = new EntidadDto<>(new Grado(), new GradoDto());

	@Override
	public List<GradoDto> listarTodos() {
		return this.getConversor().aDtoLista(this.getDao().findAll());
	}

	@Override
	public GradoDto buscarPorId(Integer id) {
		return this.getConversor().aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<GradoDto> buscarPorNombre(String nombre) {
		return this.getConversor().aDtoLista(this.getDao().findByNombre(nombre));
	}

	@Override
	public GradoDto guardarNuevo(GradoDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public GradoDto editar(GradoDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public void eliminiar(GradoDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}

}
