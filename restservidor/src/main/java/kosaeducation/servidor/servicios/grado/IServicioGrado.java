package kosaeducation.servidor.servicios.grado;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.grado.GradoDto;

@Service
public interface IServicioGrado {

	List<GradoDto> listarTodos();

	GradoDto buscarPorId(Integer id);

	List<GradoDto> buscarPorNombre(String nombre);

	GradoDto guardarNuevo(GradoDto dto);

	GradoDto editar(GradoDto dto);

	void eliminiar(GradoDto dto);
}
