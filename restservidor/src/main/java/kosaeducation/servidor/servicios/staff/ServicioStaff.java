package kosaeducation.servidor.servicios.staff;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.IStaffDao;
import kosaeducation.servidor.modelos.staff.Staff;
import kosaeducation.servidor.modelos.staff.StaffDto;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioStaff implements IServicioStaff {

	@Autowired
	private IStaffDao dao;

	List<StaffDto> listadto;

	EntidadDto<Staff, StaffDto> conversor = new EntidadDto<>(new Staff(), new StaffDto());

	public List<StaffDto> listarTodos() {

		return conversor.aDtoLista(this.getDao().findAll());
	}

	@Override
	public StaffDto buscarPorId(Integer id) {

		return conversor.aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<StaffDto> buscarPorNombre(String nombre) {
		return conversor.aDtoLista(this.getDao().findByNombre(nombre));
	}

	@Override
	public StaffDto guardarNuevo(StaffDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.conversor.aEntidad(dto)));
	}

	@Override
	public StaffDto editar(StaffDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.conversor.aEntidad(dto)));
	}

	@Override
	public void eliminiar(StaffDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}

}
