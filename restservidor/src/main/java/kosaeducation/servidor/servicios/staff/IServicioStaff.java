package kosaeducation.servidor.servicios.staff;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.staff.StaffDto;

@Service
public interface IServicioStaff {

	List<StaffDto> listarTodos();

	StaffDto buscarPorId(Integer id);

	List<StaffDto> buscarPorNombre(String nombre);

	StaffDto guardarNuevo(StaffDto dto);

	StaffDto editar(StaffDto dto);

	void eliminiar(StaffDto dto);
}
