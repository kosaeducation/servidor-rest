package kosaeducation.servidor.servicios.asignatura;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kosaeducation.servidor.dao.IAsignaturaDao;
import kosaeducation.servidor.modelos.asignatura.Asignatura;
import kosaeducation.servidor.modelos.asignatura.AsignaturaDto;
import kosaeducation.servidor.utilidades.EntidadDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class ServicioAsignatura implements IServicioAsignatura {

	@Autowired
	private IAsignaturaDao dao;

	List<AsignaturaDto> listadto;

	EntidadDto<Asignatura, AsignaturaDto> conversor = new EntidadDto<>(new Asignatura(), new AsignaturaDto());

	@Override
	public List<AsignaturaDto> listarTodos() {
		return this.getConversor().aDtoLista(this.getDao().findAll());
	}

	@Override
	public AsignaturaDto buscarPorId(Integer id) {
		return this.getConversor().aDto(this.getDao().findById(id).get());
	}

	@Override
	public List<AsignaturaDto> buscarPorCursoGrado(Integer idCursogrado) {
		return this.getConversor().aDtoLista(this.getDao().findByCursogrado(idCursogrado));
	}

	@Override
	public List<AsignaturaDto> buscarPorGrado(Integer idGrado) {
		return this.getConversor().aDtoLista(this.getDao().findByGrado(idGrado));
	}

	@Override
	public List<AsignaturaDto> buscarPorProfesor(Integer idProfesor) {
		return this.getConversor().aDtoLista(this.getDao().findByProfesor(idProfesor));
	}
	
	@Override
	public AsignaturaDto buscarPorNombre(String nombre) {
		return this.getConversor().aDto(this.getDao().findByNombre(nombre));
	}
	
	@Override
	public AsignaturaDto guardarNuevo(AsignaturaDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public AsignaturaDto editar(AsignaturaDto dto) {
		return this.getConversor().aDto(this.getDao().save(this.getConversor().aEntidad(dto)));
	}

	@Override
	public void eliminiar(AsignaturaDto dto) {
		this.getDao().delete(this.getConversor().aEntidad(dto));
	}

}
