package kosaeducation.servidor.servicios.asignatura;

import java.util.List;

import org.springframework.stereotype.Service;

import kosaeducation.servidor.modelos.asignatura.AsignaturaDto;

@Service
public interface IServicioAsignatura {

	List<AsignaturaDto> listarTodos();

	AsignaturaDto buscarPorId(Integer id);

	List<AsignaturaDto> buscarPorCursoGrado(Integer idCursogrado);

	List<AsignaturaDto> buscarPorGrado(Integer idGrado);

	List<AsignaturaDto> buscarPorProfesor(Integer idProfesor);
	
	AsignaturaDto buscarPorNombre(String nombre);

	AsignaturaDto guardarNuevo(AsignaturaDto dto);

	AsignaturaDto editar(AsignaturaDto dto);

	void eliminiar(AsignaturaDto dto);
}
