package kosaeducation.servidor.modelos.tarea;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import kosaeducation.servidor.modelos.asignatura.Asignatura;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "tareas")
public class Tarea {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTarea;
	private String nombre;
	private String descripcion;
	private Date fechaCreacion;
	private Date fechaLimite;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idAsignatura", referencedColumnName = "idAsignatura")
	private Asignatura asignatura;
}
