package kosaeducation.servidor.modelos.tarea;

import java.sql.Date;

import kosaeducation.servidor.modelos.asignatura.Asignatura;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class TareaDto {

	private Integer idTarea;
	private String nombre;
	private String descripcion;
	private Date fechaCreacion;
	private Date fechaLimite;
	private Asignatura asignatura;

}
