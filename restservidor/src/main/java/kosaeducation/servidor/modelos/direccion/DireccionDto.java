package kosaeducation.servidor.modelos.direccion;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DireccionDto {

	private Integer idDireccion;
	private String calle;
	private String numero;
	private String bloque;
	private String escalera;
	private String piso;
	private String puerta;
	private String codigoPostal;
	private String ciudad;
	private String pais;

	public DireccionDto(Direccion entidad) {
		this.idDireccion = entidad.getIdDireccion();
		this.calle = entidad.getCalle();
		this.numero = entidad.getNumero();
		this.bloque = entidad.getBloque();
		this.escalera = entidad.getEscalera();
		this.piso = entidad.getPiso();
		this.puerta = entidad.getPuerta();
		this.codigoPostal = entidad.getCodigoPostal();
		this.ciudad = entidad.getCiudad();
		this.pais = entidad.getPais();
	}
	
	
	public Direccion conversonEntidad() {
		return new Direccion(idDireccion, calle, numero, bloque, escalera, piso, puerta, codigoPostal, ciudad, pais);
	}

}
