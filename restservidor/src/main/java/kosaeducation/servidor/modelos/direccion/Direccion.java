package kosaeducation.servidor.modelos.direccion;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "direcciones")
public class Direccion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDireccion;

	@NotNull
	@Length(max = 255)
	private String calle;

	@NotNull
	@Length(max = 4)
	private String numero;

	@Length(max = 4)
	private String bloque;

	@Length(max = 4)
	private String escalera;

	@Length(max = 4)
	private String piso;

	@Length(max = 4)
	private String puerta;

	@NotNull
	@Length(max = 5)
	private String codigoPostal;

	@NotNull
	@Length(max = 25)
	private String ciudad;

	@NotNull
	@Length(max = 25)
	private String pais;

}
