package kosaeducation.servidor.modelos.asignatura;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import kosaeducation.servidor.modelos.cursogrado.CursoGrado;
import kosaeducation.servidor.modelos.staff.Staff;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "asignaturas")
public class Asignatura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idAsignatura;

	@NotNull
	@Length(max = 255)
	private String nombre;

	@NotNull
	@Length(max = 255)
	@ManyToOne
	@JoinColumn(name = "idCursoGrado")
	private CursoGrado cursogrado;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idProfesor", referencedColumnName = "idStaff")
	private Staff profesor;
}
