package kosaeducation.servidor.modelos.asignatura;

import kosaeducation.servidor.modelos.cursogrado.CursoGradoDto;
import kosaeducation.servidor.modelos.staff.StaffDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AsignaturaDto {

	private Integer idAsignatura;
	private String nombre;
	private CursoGradoDto cursogrado;
	private StaffDto profesor;
}
