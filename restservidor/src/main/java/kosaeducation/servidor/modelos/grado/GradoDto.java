package kosaeducation.servidor.modelos.grado;

import kosaeducation.servidor.modelos.staff.StaffDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class GradoDto {

	private Integer idGrado;
	private String nombre;
	private StaffDto coordinador;
}
