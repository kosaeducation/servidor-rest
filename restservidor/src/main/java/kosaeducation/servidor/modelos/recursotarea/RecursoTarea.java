package kosaeducation.servidor.modelos.recursotarea;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import kosaeducation.servidor.modelos.tarea.Tarea;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "recursos_tareas")
public class RecursoTarea {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idRecurso;
	
	private String uri;
	
	private String nombrearchivo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idTarea", referencedColumnName = "idTarea")
	private Tarea tarea;
	
}
