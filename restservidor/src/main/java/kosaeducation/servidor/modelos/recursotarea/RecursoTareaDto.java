package kosaeducation.servidor.modelos.recursotarea;

import kosaeducation.servidor.modelos.tarea.TareaDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class RecursoTareaDto {

	private Integer idRecurso;
	private String uri;
	private String nombrearchivo;
	private TareaDto tarea;
}
