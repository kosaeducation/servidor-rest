package kosaeducation.servidor.modelos.estudiante;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import kosaeducation.servidor.modelos.cursogrado.CursoGrado;
import kosaeducation.servidor.modelos.direccion.Direccion;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "estudiantes")
public class Estudiante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idEstudiante;

	@NotNull
	@Length(max = 50)
	private String nombre;

	@Length(max = 50)
	private String segundoNombre;

	@NotNull
	@Length(max = 100)
	private String primerApellido;

	@NotNull
	@Length(max = 100)
	private String segundoApellido;

	@NotNull
	@Length(max = 9)
	private String dni;

	@NotNull
	private Date fechaNacimiento;

	@NotNull
	@Length(max = 1)
	private String genero;

	@NotNull
	@Length(max = 75)
	private String email;

	@NotNull
	@Length(max = 12)
	private String telefonoMovil;

	@Length(max = 12)
	private String telefonoFijo;

	@NotNull
	private Date fechaRegistro;

	private Date fechaBaja;

	@Length(max = 255)
	private String observaciones;

	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idDireccion", referencedColumnName = "idDireccion")
	private Direccion direccion;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idCursoGrado")
	private CursoGrado cursogrado;

}
