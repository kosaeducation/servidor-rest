package kosaeducation.servidor.modelos.estudiante;

import java.sql.Date;

import kosaeducation.servidor.modelos.cursogrado.CursoGradoDto;
import kosaeducation.servidor.modelos.direccion.DireccionDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstudianteDto {

	private Integer idEstudiante;
	private String nombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;
	private String dni;
	private Date fechaNacimiento;
	private String genero;
	private String email;
	private String telefonoMovil;
	private String telefonoFijo;
	private Date fechaRegistro;
	private Date fechaBaja;
	private String observaciones;
	private DireccionDto direccion;
	private CursoGradoDto cursogrado;


}
