package kosaeducation.servidor.modelos.horalectiva;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "horaslectivas")
public class HoraLectiva {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idHora;

	@NotNull
	@Length(max = 5)
	private String horas;

}
