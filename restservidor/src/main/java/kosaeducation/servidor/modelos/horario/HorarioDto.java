package kosaeducation.servidor.modelos.horario;

import kosaeducation.servidor.modelos.asignatura.AsignaturaDto;
import kosaeducation.servidor.modelos.aulafisica.AulaFisicaDto;
import kosaeducation.servidor.modelos.cursogrado.CursoGradoDto;
import kosaeducation.servidor.modelos.diasemana.DiaSemanaDto;
import kosaeducation.servidor.modelos.horalectiva.HoraLectivaDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class HorarioDto{

	private Integer idHorario;
	private AsignaturaDto asignatura;
	private DiaSemanaDto diasemana;
	private HoraLectivaDto hora;
	private CursoGradoDto cursogrado;
	private AulaFisicaDto aulafisica;
}
