package kosaeducation.servidor.modelos.horario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "horarios")
public class Horario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idHorario;

	@NotNull
	@Column(name = "idAsignatura")
	private Integer idAsignatura;
	
	@NotNull
	@Column(name = "idDiaSemana")
	private Integer idDiaSemana;
	
	@NotNull
	@Column(name = "idHora")
	private Integer idHora;
	
	@NotNull
	@Column(name = "idCursosGrados")
	private Integer idCursosGrados;

	@NotNull
	@Column(name = "idAulaFisica")
	private Integer idAulaFisica;
	

}
