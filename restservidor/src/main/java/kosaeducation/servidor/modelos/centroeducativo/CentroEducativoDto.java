package kosaeducation.servidor.modelos.centroeducativo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CentroEducativoDto {

	private Integer idCentro;
	private String nombre;
	private String nif;
	private String calle;
	private String numero;
	private String telefono1;
	private String telefono2;
	private String codigoPostal;
	private String ciudad;
	private String pais;

}
