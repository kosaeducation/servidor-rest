package kosaeducation.servidor.modelos.centroeducativo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "centroeducativo")
public class CentroEducativo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCentro;

	@NotNull
	@Length(max = 255)
	private String nombre;

	private String nif;

	@NotNull
	@Length(max = 255)
	private String calle;

	@NotNull
	@Length(max = 255)
	private String numero;

	@NotNull
	@Length(max = 12)
	private String telefono1;

	@Length(max = 12)
	private String telefono2;
	
	@NotNull
	@Length(max = 150)
	private String email;
	
	@NotNull
	@Length(max = 5)
	private String codigoPostal;

	@NotNull
	@Length(max = 25)
	private String ciudad;

	@NotNull
	@Length(max = 25)
	private String pais;

}
