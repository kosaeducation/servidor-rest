package kosaeducation.servidor.modelos.aulafisica;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AulaFisicaDto{

	private Integer idAulaFisica;
	private String nombre;
	
}
