package kosaeducation.servidor.modelos.cursogrado;

import kosaeducation.servidor.modelos.curso.Curso;
import kosaeducation.servidor.modelos.grado.Grado;
import kosaeducation.servidor.modelos.staff.StaffDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CursoGradoDto {

	private Integer idCursoGrado;
	private Curso curso;
	private Grado grado;
	private StaffDto tutor;

}
