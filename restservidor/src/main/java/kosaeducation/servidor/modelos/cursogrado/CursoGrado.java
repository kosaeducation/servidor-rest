package kosaeducation.servidor.modelos.cursogrado;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import kosaeducation.servidor.modelos.curso.Curso;
import kosaeducation.servidor.modelos.grado.Grado;
import kosaeducation.servidor.modelos.staff.Staff;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "cursosgrados")
public class CursoGrado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCursoGrado;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idCurso")
	private Curso curso;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idGrado")
	private Grado grado;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idTutor", referencedColumnName = "idStaff")
	private Staff tutor;
}
