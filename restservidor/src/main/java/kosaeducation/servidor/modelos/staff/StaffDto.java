package kosaeducation.servidor.modelos.staff;

import java.sql.Date;

import kosaeducation.servidor.modelos.direccion.DireccionDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StaffDto {

	private Integer idStaff;
	private String nombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;
	private String dni;
	private String nss;
	private Date fechaNacimiento;
	private String genero;
	private String email;
	private String telefonoMovil;
	private String telefonoFijo;
	private Date fechaRegistro;
	private Date fechaBaja;
	private String observaciones;
	private DireccionDto direccion;


}
