package kosaeducation.servidor.modelos.diasemana;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DiaSemanaDto{

	private Integer idDiaSemana;
	private String nombre;
	
}
