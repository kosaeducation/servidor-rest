package kosaeducation.servidor.modelos.diasemana;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "diassemanas")
public class DiaSemana {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDiaSemana;

	@NotNull
	@Length(max = 10)
	private String nombre;

}
